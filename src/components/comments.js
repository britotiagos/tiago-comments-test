import React, { useContext } from "react"
import CommentsContext from "../context/comments"

const Comments = () => {
  const { comment, comments, onChange, onSubmit } = useContext(CommentsContext)

  return (
    <div>
      <form onSubmit={onSubmit}>
        <label>
          Comment
          <input
            onChange={onChange}
            value={comment}
            style={{ display: "block" }}
            name="comment"
          />
        </label>
        <button type="submit">Add Comment</button>
      </form>
      <div>
        <h1>Comment List</h1>
        <ul>
          {comments.map((cmnt, index) => (
            <li key={index}>
              <p>{cmnt}</p>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Comments
