import { useState, useReducer } from "react"

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return state + 1
    default:
      return state
  }
}

const useComments = () => {
  const [comment, setComment] = useState("")
  const [comments, setComments] = useState([])
  const [numberOfComments, dispatch] = useReducer(reducer, 0)

  const onChange = event => {
    setComment(event.target.value)
  }

  const onSubmit = event => {
    event.preventDefault()
    setComments([...comments, comment])
    setComment("")
    dispatch({ type: "increment" })
  }

  return { comment, comments, numberOfComments, onChange, onSubmit }
}

export default useComments
