import React, { useContext } from "react"
import { Link } from "gatsby"
import CommentsContext from "../context/comments"
import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => {
  const { comments, numberOfComments } = useContext(CommentsContext)
  return (
    <Layout>
      <SEO title="Page two" />
      Second page, still those beatiful comments
      <h6>There are {numberOfComments} comments</h6>
      <h1>Last 5 great comments</h1>
      <ul>
        {comments
          .slice(-5)
          .reverse()
          .map((cmnt, index) => (
            <li key={index}>
              <p>{cmnt}</p>
            </li>
          ))}
      </ul>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  )
}
export default SecondPage
