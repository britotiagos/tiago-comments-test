import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Comments from "../components/comments"

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <h1>HEY GORGEOUS!!! </h1>
      <Comments />

      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  )
}

export default IndexPage
